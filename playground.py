# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals
import re
import numpy as np

#char to int
c2i = {"a": 0, "b": 1, "c": 2, "d": 3, "e": 4, "f": 5, "g": 6, "h": 7}
#char to piece
c2p = {
    "k": "King",
    "q": "Queen",
    "r": "Rook",
    "b": "Bishop",
    "n": "Knight",
    "": "Pawn"
}


class CoordinateException(Exception):
    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        Exception.__init__(self, message)

def empty_row():
    return [Empty() for i in range(0, 8)]

def to_coordinates(string):
    """
    Takes coordinates in string (as c4, e8, a1) and returns array of coordinates
    """
    if not re.match('^[a-hA-H][1-8]$', string):
        raise CoordinateException("Coordinates {} are not recognized!".format(string))
    x = string[0]
    y = string[1]

    return [int(c2i[x.lower()]), int(y)-1]




def piece_factory(piece, black=False):
    #Not checking for stuff as its internal function
    if piece != "":
        piece = piece[0].lower()
    #This is ugly
    return globals()[c2p[piece]](black)


class Chessboard(object):
    u"""Chessboard, contains data of board and pieces in game"""
    def __init__(self):
        self.board = [empty_row() for i in range(0, 8)]
        self.player = 0

    def __repr__(self):
        output = unicode(" |a|b|c|d|e|f|g|h|\n")
        for i, row in enumerate(self.board):
            output += "{}|".format(unicode(i))
            for cell in row:
                output += "{}|".format(unicode(cell))
            output += "\n"
        return unicode(output)

    def new_game(self):
        """Prepares the board for new game"""
        white = {
            "": ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
            "R": ["a1", "h1"],
            "N": ["b1", "g1"],
            "B": ["c1", "f1"],
            "K": ["e1"],
            "Q": ["d1"]
        }
        black = {
            "": ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
            "R": ["a8", "h8"],
            "N": ["b8", "g8"],
            "B": ["c8", "f8"],
            "K": ["e8"],
            "Q": ["d8"]
        }

        for piece, positions in white.items():
            for position in positions:
                self._placePiece(piece, position)

        #bit WET, but no time
        for piece, positions in black.items():
            for position in positions:
                self._placePiece(piece, position, True)


    def is_end(self):
        """Checks if this game is ended (checkmate, draw) """
        return False

    def make_move(self, user_input):
        """All the logic behind moving figures"""
        raise NotImplementedError
        self.player = not self.player

    def now_plays(self):
        return "black" if self.player else "white"

    def _placePiece(self, piece_str, place, black=False):
        coordinates = to_coordinates(place)
        piece = piece_factory(piece_str, black)
        self.board[coordinates[0]][coordinates[1]] = piece

class Empty():
    def __repr__(self):
        return " "



class Piece(object):
    def __init__(self, black=False):
        self.black = bool(black)
        self.max_steps = 0
        self.diagonally = False
        self.vertically = False
        self.horizontally = False
        self.identifier = "X"

    def __repr__(self):
        return unicode(self.identifier)

    def can_move(self, curr_position, vector):
        """Returns bool if Piece can move this vector
            curr_position = [x,y] -- current position
            vector = [int,int] -- how much it should move (horizontal, vertical)
        """
        if vector[0] == 0 and vector[1] == 0:
            #No movement
            return False

        if self.diagonnaly:
            if vector[0] == 0 or vector[1] == 0:
                #can't happen
                return False
            if vector[0] != vector[1]:
                #It's not diagonally
                return False

        if self.vertically and self.horizontally:
            if vector[0] != 0 and vector != 0:
                #it's not vertohorizontally
                return False
            if vector[0] > self.max_steps or vector[1] > self.max_steps:
                #Too long step
                return False

        #We will implement pawn by itself, no need for checking only vertical moves

        if self._step_overboard(curr_position, vector):
            return False

        return True

    def _step_overboard(self, curr_position, vector):
        """Returns bool if movement results in being out of chessboard
            curr_position = [x,y] -- current position
            vector = [int,int] -- how much it should move (horizontal, vertical)
        """
        resulting_place = np.array(curr_position) + np.array(vector)
        if 0 > resulting_place[0] > 8 or 0 > resulting_place[0] > 8:
            return True
        return False


class King(Piece):
    def __init__(self, black=False):
        self.black = bool(black)
        self.max_steps = 1
        self.diagonally = True
        self.vertically = True
        self.horizontally = True
        if self.black:
            self.identifier = "K"
        else:
            self.identifier = "k"

    #Todo: implement castling


class Queen(Piece):
    def __init__(self, black=False):
        self.black = bool(black)
        self.max_steps = 8
        self.diagonally = True
        self.vertically = True
        self.horizontally = True
        if self.black:
            self.identifier = "Q"
        else:
            self.identifier = "q"


class Rook(Piece):
    def __init__(self, black=False):
        self.black = bool(black)
        self.max_steps = 8
        self.diagonally = False
        self.vertically = True
        self.horizontally = True
        if self.black:
            self.identifier = "R"
        else:
            self.identifier = "r"

class Bishop(Piece):
    def __init__(self, black=False):
        self.black = bool(black)
        self.max_steps = 8
        self.diagonally = True
        self.vertically = False
        self.horizontally = False
        if self.black:
            self.identifier = "B"
        else:
            self.identifier = "b"


class Knight(Piece):
    def __init__(self, black=False):
        self.black = bool(black)
        self.max_steps = 8
        self.diagonally = True
        self.vertically = False
        self.horizontally = False
        if self.black:
            self.identifier = "K"
        else:
            self.identifier = "k"

    def can_move(self, curr_position, vector):
        #special movement
        raise NotImplementedError


class Pawn(Piece):
    def __init__(self, black=False):
        self.black = bool(black)
        self.max_steps = 8
        self.diagonally = True
        self.vertically = False
        self.horizontally = False
        if self.black:
            self.identifier = "P"
        else:
            self.identifier = "p"

    def can_move(self, curr_position, vector):
        #special movements
        raise NotImplementedError