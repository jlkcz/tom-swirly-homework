#!venv/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals
import sys
import playground


#Todo:
#Basic movement
#end checking
#special moves (castling, pawn at the end)
#

if __name__ == "__main__":
    try:
        chessboard = playground.Chessboard()
        chessboard.new_game()
        print("Welcome to console chess!")
        print(chessboard)
        while not chessboard.is_end():
            print("Now plays: {}".format(chessboard.now_plays()))
            print(chessboard)
            move = raw_input("Please insert your move: ")
    except KeyboardInterrupt:
        print("\n"+"="*50)
        print("Got Ctrl+C, ending")
        print("This is your last state of board:")
        print(chessboard)
        sys.exit(0)